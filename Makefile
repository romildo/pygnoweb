# Makefile

PKGVERSION = 0.1

TEXFLAGS =
LATEXMK = latexmk
ICONC = icont
CPIF = | cpif		# change to ">" to insure all sources always made

PYGSTYLE = default
# PYGSTYLE = bw

#KNOWEBDIR = /var/tmp/JoeRiel-knoweb-b91b036

PKGNAME = pygnoweb

PACKAGE = $(PKGNAME).sty $(PKGNAME).py pygnoweb-totex.nw test.nw README Makefile

.SUFFIXES: .nw .icn .tex .pdf

%: %.icn
	$(ICONC) $<

%.icn: %.nw
	notangle -L'#line %-1L "%F"%N' $< $(CPIF) $@

%.tex: %.nw
	noweave \
	  -autodefs c \
	  -filter stripmodeline \
	  -filter "inlinecomments commmentre=\"%[|]\" commentshow=\"%\"" \
	  -filter "tee $<.ir" \
	  -filter "./pygnoweb.py" \
	  -backend ./pygnoweb-totex \
	  -delay -index $< > $@

%.pdf: %.tex pygstyle.sty # %.bbl
	$(LATEXMK) -pdf $(TEXFLAGS) $<
	#pdflatex $<
	#pdflatex $<

pygstyle.sty:
	pygmentize -S $(PYGSTYLE) -f latex > $@


.PHONY: all doc dist clean cleanall test

all: $(PKGNAME).sty $(PKGNAME).py pygnoweb-totex

doc: $(PKGNAME).pdf

test: $(PKGNAME).sty $(PKGNAME).py pygnoweb-totex test.pdf

dist: $(PACKAGE)
	$(RM) $(PKGNAME)-$(PKGVERSION).tar.gz
	tar -zcvf $(PKGNAME)-$(PKGVERSION).tar.gz $(PACKAGE)

clean:
	@$(RM) *.aux *.log *.out *.toc *.ilg *.glo *.gls *.lol
	@$(RM) *.fdb_latexmk
	@$(RM) *.pdf
	@$(RM) *.ir
	@$(RM) pygstyle.sty
	@$(RM) pygnoweb-totex

cleanall: clean
	@#$(RM) $(PKGNAME).sty
	@$(RM) *.pdf *.dvi
	@$(RM) $(PKGNAME)-$(PKGVERSION).tar.gz

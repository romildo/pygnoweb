#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""
    pygnoweb
    ~~~~~~~~

    pygnoweb is a filter to do syntax highlighting of code chunks from
    the noweb literate programming system. It operates on the
    intermediate representation of do document. See The noweb Hacker's
    Guide for further information.

    :copyright: Copyright 2010 by José Romildo Malaquias
    :license: BSD, see LICENSE for details
"""

__version__ = '0.1'
__docformat__ = 'restructuredtext'

import sys
import getopt
import re
from os.path import splitext

from pygments import highlight
from pygments.styles import get_style_by_name
from pygments.lexers import get_lexer_by_name
from pygments.formatters.latex import LatexFormatter# , LatexEmbededLexer
from pygments.util import get_bool_opt, get_int_opt
from pygments.lexer import Lexer


class LatexEmbededLexer(Lexer):
    r"""

    This is a lexer that looks for escaped LaTeX code segments inside
    the text to be scanned.

    This lexer takes one lexer as argument, the lexer for the language
    being formatted, and the left and right delimiters for escaped text.

    First everything is scanned using the language lexer to obtain
    strings and comments. All other consecutive tokens are merged and
    the resulting text is scanned for escaped segments, which are given
    the Token.Escape type. Finally text that is not escaped is scanned
    again with the language lexer.
    """
    def __init__(self, left, right, lang, **options):
        self.left = left
        self.right = right
        self.lang = lang
        Lexer.__init__(self, **options)

    def get_tokens_unprocessed(self, text):
        buf = ''
        for i, t, v in self.lang.get_tokens_unprocessed(text):
            if t in Token.Comment or t in Token.String:
                if len(buf) > 0:
                    for x in self.get_tokens_aux(idx, buf):
                        yield x
                    buf = ''
                yield i, t, v
            else:
                if len(buf) == 0:
                    idx = i;
                buf = buf + v
        if len(buf) > 0:
            for x in self.get_tokens_aux(idx, buf):
                yield x

    def get_tokens_aux(self, index, text):
        while len(text) > 0:
            a, sep1, text = text.partition(self.left)
            if len(a) > 0:
                for i, t, v in self.lang.get_tokens_unprocessed(a):
                    yield index + i, t, v
                    index = index + len(a)
            if len(sep1) > 0:
                b, sep2, text = text.partition(self.right)
                if len(sep2) > 0:
                    yield index + len(sep1), Token.Escape, b
                    index = index + len(sep1) + len(b) + len(sep2)
                else:
                    yield index, Token.Error, sep1
                    index = index + len(sep1)
                    text = b



_fmter = LatexFormatter()


def pygmentize_chunk(language, text):
    if not language:
        language = 'c'
    try:
        lexer = get_lexer_by_name(language)
    except ClassNotFound, err:
        print >>sys.stderr, 'Error:', err
        return ""

    global _fmter

    if sys.version_info < (3,):
        # use terminal encoding; Python 3's terminals already do that
        lexer.encoding = getattr(sys.stdin, 'encoding',
                                 None) or 'ascii'
        _fmter.encoding = getattr(sys.stdout, 'encoding',
                                  None) or 'ascii'

    x = highlight(text, lexer, _fmter)

    m = re.match(r'\\begin\{Verbatim}(.*)\n([\s\S]*?)\n\\end\{Verbatim}(\s*)\Z',
                 x)
    if m:
        x = m.group(2)
        # x = re.sub(r'^ ', r'\\makebox[0pt]{\\phantom{Xy}} ', x)
        # x = re.sub(r' ', '~', x)
    else:
        x = text
    # return x.replace('\n', '\\newline\n')
    return x

def merge(text, more, outfile):
    text = text.split('\n')
    i = 0
    j = 0
    while i < len(text) and j < len(more):
        if more[j][0] <= i:
            outfile.write(more[j][1])
            j = j + 1
        else:
            outfile.write('@text')
            outfile.write(text[i])
            outfile.write('\n')
            i = i + 1
    while i < len(text):
        outfile.write('@text')
        outfile.write(text[i])
        outfile.write('\n')
        i = i + 1
    while j < len(more):
        outfile.write(more[j][1])
        j = j + 1

def noweb_filter(language, infile, outfile):
    """
    Filter ``doc``.
    """
    lang = language
    chunk = None
    missed = []
    for line in infile:
        if line.startswith('@begin code'):
            lang = language
            chunk = ''
            lineno = 0
            missed = []
            outfile.write(line)
        elif line.startswith('@end code'):
            merge(pygmentize_chunk(lang, chunk), missed, outfile)
            # outfile.write(pygmentize_chunk(language, chunk))
            outfile.write(line)
            chunk = None
            lineno = -1
        elif chunk != None:
            if line.startswith('@text'):
                chunk = chunk + line[5:]
                lineno = lineno + 1
            # elif line.startswith('@nl'):
            #     chunk = chunk + '\n'
            #     lineno = lineno + 1
            else:
                if line.startswith('@language'):
                    lang = line[10:]
                missed.append((lineno,line))
        else:
            outfile.write(line)



USAGE = """\
Usage: %s [-l <language>]
       %s -h | -V

This program is a filter that reads the standard input, process it, and
outputs to standarad output. It is suitable for use in a noweb filter
pipeline.

Standard input should be a noweb internal representation of a literate
document. Each code chunk from the standard input will be extract and
given to pygments to be highlighted. The highlighted version will then
be inserted into the output.

The -l <language> option specifies the programming language in which the
code chunks was written. If not specified in the command line, the
language specification, if any, from the code chunks will be used.

The -h option prints this help.

The -V option prints the package version.
"""


def main(args = sys.argv):
    """
    Main command line entry point.
    """
    usage = USAGE % ((args[0],) * 2)

    try:
        popts, args = getopt.getopt(args[1:], 'l:hV')
    except getopt.GetoptError, err:
        print >>sys.stderr, usage
        return 2
    opts = {}
    for opt, arg in popts:
        opts[opt] = arg

    if opts.pop('-h', None) is not None:
        print usage
        return 0

    if opts.pop('-V', None) is not None:
        print 'pygnoweb version %s, (c) 2010 by José Romildo Malaquias.' % __version__
        return 0

    language = opts.pop('-l', None)

    noweb_filter(language, sys.stdin, sys.stdout)

    return 0


if __name__ == '__main__':
    try:
        sys.exit(main(sys.argv))
    except KeyboardInterrupt:
        sys.exit(1)
